//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <math.h>
#include "graphics.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
float a,b,c,x,y,imw,imh,im2w,im2h;
int i, indgrid=0;
AnsiString fname;
TLabel *Lab1[5],*Lab2[5];
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
// ������������� ����������� �����
Image1->Canvas->Brush->Color=clWhite;
Image1->Canvas->Pen->Color=clBlack;
imw=Image1->Width;
imh=Image1->Height;
Image1->Canvas->Rectangle(0,0,imw,imh);

im2w=Image2->Width;
im2h=Image2->Height;
Image2->Canvas->Brush->Color=clWhite;
Image2->Canvas->Pen->Color=clBlack;
Image2->Canvas->Rectangle(0,0,im2w,im2h);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
Image1->Canvas->Pen->Color=(TColor)RGB(255,0,0);
Image1->Canvas->Pen->Width=2;
Image1->Canvas->MoveTo(0,0);
x=0.00001;
while (x<imw)
{

	y=imh/2*sin(x/10)/(x/10)+imh/2;
    Image1->Canvas->LineTo(x,imh-y);
    x=x++;
}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
Image1->Canvas->FillRect(Rect(0,0,imw,imh));
Image1->Canvas->Pen->Color=clBlack;
Image1->Canvas->Pen->Width=1;
Image1->Canvas->Rectangle(0,0,imw,imh);
if(indgrid==1) {
indgrid=0;
Button3Click(Button3);
}
if(Lab1 != 0){
for (i = 0; i < 5; i++) {
Lab1[i]->Caption="";
Lab2[i]->Caption="";
}
}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
float dx,dy;
dx = imw/4; dy=imh/4;
Image1->Canvas->Pen->Width=1;
if(indgrid==0) {
Image1->Canvas->Pen->Color=clBlack;
indgrid=1;}
else {
Image1->Canvas->Pen->Color=clWhite;
indgrid=0;}
for (i = 1; i < 4; i++) {
y=dy*i;
Image1->Canvas->MoveTo(1,y);
for (x = 1; x < imw; x++) {
Image1->Canvas->LineTo(x,y);
}
}
for (i = 1; i < 4; i++) {
x=dx*i;
Image1->Canvas->MoveTo(x,1);
for (y = 1; y < imh; y++) {
Image1->Canvas->LineTo(x,y);
}
}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
//TLabel *Lab1[5];
for (i = 0; i < 5; i++) {
Lab1[i] = new TLabel(Form1);
Lab1[i]->Parent=Form1;
Lab1[i]->Top=Image1->Top+imh+5;
Lab1[i]->Left=Image1->Left+imw/4*i-10;
//Lab1[i]->Width=20;
//Lab1[i]->Height=20;
Lab1[i]->Alignment= taCenter;
Lab1[i]->Caption=FloatToStrF(imw/4*i/10,ffFixed,5,1);
}
//TLabel *Lab2[5];
for (i = 0; i < 5; i++) {
Lab2[i] = new TLabel(Form1);
Lab2[i]->Parent=Form1;
Lab2[i]->Top=Image1->Top+imh-imh/4*i-10;
Lab2[i]->Left=Image1->Left-25;
//Lab2[i]->Width=20;
//Lab2[i]->Height=20;
Lab2[i]->Alignment= taCenter;
Lab2[i]->Caption=FloatToStrF(2./4*i-1,ffFixed,5,1);
}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
if(OpenPictureDialog1->Execute()){
fname=OpenPictureDialog1->FileName;
Image2->Picture->LoadFromFile(fname);
Image2->Proportional=True;
}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button6Click(TObject *Sender)
{
Image2->Canvas->Brush->Color=clWhite;
Image2->Canvas->Pen->Color=clBlack;
Image2->Picture->Graphic->SetSize(im2w,im2h);

Image2->Canvas->Rectangle(0,0,im2w,im2h);
//Image2->Canvas->FillRect(Rect(0,0,im2w,im2h));
}

//---------------------------------------------------------------------------

void __fastcall TForm1::N2Click(TObject *Sender)
{
Button1Click(Button1);
}
//---------------------------------------------------------------------------

